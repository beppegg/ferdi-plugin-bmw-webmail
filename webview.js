'use strict';

module.exports = Franz => {
  const getMessages = function getMessages() {
    let unreadMail = 0;
    
    let inboxText = "";
    switch (navigator.language.substring(0, 2)) {
      case "it":
        inboxText = "Posta in arrivo"
        break;
      case "en":
        inboxText = "Inbox"
        break;
    }

    unreadMail = parseInt(
      jQuery("span:contains('" + inboxText + "') + span + span")
        .first()
        .text(),
      10
    );

    Franz.setBadge(unreadMail);
  };

  Franz.loop(getMessages);
};
